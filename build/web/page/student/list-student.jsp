<%-- 
    Document   : list-student
    Created on : May 28, 2019, 8:02:09 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <a href="getForm" class="btn btn-success">Thêm mới</a>
                </div>
            </div>
            <div class="row margin-top-10">
                <div class="col-lg-12">
                    <table class="table" id="datatable">
                        <thead class="tableClass">
                            <tr>
                                <th>Mã sinh viên</th>
                                <th>Tên</th>
                                <th>Giới tính</th>
                                <th>Ngày sinh</th>
                                <th>Số điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="stu" items="${listStudent}">
                                <tr>
                                    <td><c:out value="${stu.studentCode}"/></td>
                                    <td><c:out value="${stu.name}"/></td>
                                    <td>
                                        <c:if test="${stu.gender == 0}">
                                            Nam
                                        </c:if>
                                        <c:if test="${stu.gender == 1}">
                                            Nữ
                                        </c:if>
                                    </td>
                                    <td><c:out value="${stu.dob}"/></td>
                                    <td><c:out value="${stu.phoneNumber}"/></td>
                                    <td><c:out value="${stu.addressDetail}"/></td>
                                    <td>
                                        <a href="edit?id=<c:out value='${stu.id}'/>">Sửa</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#" onclick="deleteStudent(${stu.id})">Xóa</a>                     
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </jsp:attribute>
</templace:template>