/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.controller;

import com.student.dao.AddressDao;
import com.student.dao.DistrictDao;
import com.student.dao.GetNextCodeDao;
import com.student.dao.ProvindDao;
import com.student.dao.StudentDao;
import com.student.dao.TownDao;
import com.student.model.Address;
import com.student.model.District;
import com.student.model.Provind;
import com.student.model.Student;
import com.student.model.Town;
import com.student.model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phuong Ken
 */
public class StudentController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getPathInfo().substring(1);
        StudentDao studentDao = new StudentDao();
        try {
            switch (action) {
                case "getForm":
                    showNewForm(request, response);
                    break;
                case "insert":
                    insert(request, response);
                    break;
                case "delete":
                    delete(request, response);
                    break;
                case "edit":
                    showEditForm(request, response);
                    break;
                case "update":
                    update(request, response);
                    break;
                default:
                    List<Student> listStudent = studentDao.list();
                    request.setAttribute("listStudent", listStudent);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/page/student/list-student.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        ProvindDao provindDao = new ProvindDao();
        List<Provind> listProvind = provindDao.list();
        request.setAttribute("listProvind", listProvind);
        GetNextCodeDao getNextCodeDao = new GetNextCodeDao();
        request.setAttribute("studentCode", getNextCodeDao.getStudentCode());
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/student/new-student.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        StudentDao studentDao = new StudentDao();
        AddressDao addressDao = new AddressDao();
        ProvindDao provindDao = new ProvindDao();
        DistrictDao districtDao = new DistrictDao();
        TownDao townDao = new TownDao();

        List<Provind> listProvinds = provindDao.list();
        List<District> listDistrict = districtDao.list();
        List<Town> listTowns = townDao.list();

        request.setAttribute("listProvind", listProvinds);
        request.setAttribute("listDistrict", listDistrict);
        request.setAttribute("listTown", listTowns);

        int id = Integer.parseInt(request.getParameter("id"));
        Student existingStudent = studentDao.get(id);
        Address address = addressDao.get(existingStudent.getAddressId());
        request.setAttribute("item", existingStudent);
        request.setAttribute("itemAddress", address);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/student/new-student.jsp");
        dispatcher.forward(request, response);
    }

    private void insert(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ParseException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        int gender = Integer.parseInt(request.getParameter("gender"));
        String[] datedob = request.getParameter("dob").split("-");
        Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(datedob[2] + "/" + datedob[1] + "/" + datedob[0]);
        int phoneNumber = Integer.parseInt(request.getParameter("phoneNumber"));
        int provindId = Integer.parseInt(request.getParameter("provindId"));
        int districtId = Integer.parseInt(request.getParameter("districtId"));
        int townId = Integer.parseInt(request.getParameter("townId"));
        String addressDetail = request.getParameter("addressDetail");
        long identity = Integer.parseInt(request.getParameter("identity"));
        String email = request.getParameter("email");
        int status = Integer.parseInt(request.getParameter("status"));

        StudentDao studentDao = new StudentDao();
        GetNextCodeDao codeDao = new GetNextCodeDao();
        AddressDao addressDao = new AddressDao();
        Address address = new Address(provindId, provindId, districtId, townId);
        int addressId = addressDao.insert(address);
        String studentCode = codeDao.getStudentCode();

        HttpSession session = request.getSession();
        User userLogin = (User) session.getAttribute("userLogin");

        Student student = new Student(studentCode, name, dob, addressId,
                addressDetail, identity, phoneNumber, gender, email, status, userLogin.getId(), new Date());

        boolean result = studentDao.insert(student);
        if (result == true) {
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Đã có lỗi xảy ra vui lòng kiểm tra lại");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/page/student/new-student.jsp");
            dispatcher.forward(request, response);
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ParseException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        int gender = Integer.parseInt(request.getParameter("gender"));
        String[] datedob = request.getParameter("dob").split("-");
        Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(datedob[2] + "/" + datedob[1] + "/" + datedob[0]);
        int phoneNumber = Integer.parseInt(request.getParameter("phoneNumber"));
        int provindId = Integer.parseInt(request.getParameter("provindId"));
        int districtId = Integer.parseInt(request.getParameter("districtId"));
        int townId = Integer.parseInt(request.getParameter("townId"));
        String addressDetail = request.getParameter("addressDetail");
        long identity = Integer.parseInt(request.getParameter("identity"));
        String email = request.getParameter("email");
        int status = Integer.parseInt(request.getParameter("status"));

        StudentDao studentDao = new StudentDao();
        GetNextCodeDao codeDao = new GetNextCodeDao();
        AddressDao addressDao = new AddressDao();

        int addressId = Integer.parseInt(request.getParameter("addressId"));
        Address address = new Address(addressId, provindId, districtId, townId);
        addressDao.update(address);
        String studentCode = codeDao.getStudentCode();

        HttpSession session = request.getSession();
        User userLogin = (User) session.getAttribute("userLogin");

        Student student = new Student(id, studentCode, name,
                dob, addressId, addressDetail, identity,
                phoneNumber, gender, email, status, addressId, dob, userLogin.getId(), new Date(), gender, gender);
        boolean result = studentDao.update(student);
        if (result == true) {
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Đã có lỗi xảy ra vui lòng kiểm tra lại");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/page/student/new-student.jsp");
            dispatcher.forward(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ParseException, ServletException {

        int id = Integer.parseInt(request.getParameter("id"));
        boolean result = false;
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        StudentDao studentDao = new StudentDao();
        result = studentDao.delete(id);

        if (result == true) {
            response.getWriter().write("true");
        } else {
            response.getWriter().write("false");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
