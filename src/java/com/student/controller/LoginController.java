/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.controller;

import com.student.dao.LoginDao;
import com.student.model.User;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phuong Ken
 */
public class LoginController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/login.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        LoginDao loginDao = new LoginDao();
        HttpSession session = request.getSession();
        User user = loginDao.Login(userName, password);
        if (user != null) {
            session.setAttribute("userLogin", user);
            response.sendRedirect("");
        } else {
            request.setAttribute("error", "Tài khoản hoặc mật khẩu sai.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/page/login.jsp");
            dispatcher.forward(request, response);
        }
    }

}
