/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.controller;

import com.student.dao.MarkDao;
import com.student.dao.StudentDao;
import com.student.dao.SubjectsDao;
import com.student.model.Mark;
import com.student.model.Student;
import com.student.model.Subjects;
import com.student.model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phuong Ken
 */
public class MarkController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getPathInfo().substring(1);
        MarkDao markDao = new MarkDao();
        try {
            switch (action) {
                case "getForm":
                    showNewForm(request, response);
                    break;
                case "insert":
                    insert(request, response);
                    break;
                case "delete":
                    delete(request, response);
                    break;
                case "edit":
                    showEditForm(request, response);
                    break;
                case "update":
                    update(request, response);
                    break;
                default:
                    List<Mark> listMark = markDao.list();
                    request.setAttribute("listMark", listMark);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/page/mark/list-mark.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        StudentDao studentDao = new StudentDao();
        SubjectsDao subjectsDao = new SubjectsDao();
        
        List<Student> listStudent = studentDao.list();
        List<Subjects> listSubjects = subjectsDao.list();
        
        request.setAttribute("listStudent", listStudent);
        request.setAttribute("listSubjects", listSubjects);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/mark/new-mark.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));
        StudentDao studentDao = new StudentDao();
        SubjectsDao subjectsDao = new SubjectsDao();
        
        List<Student> listStudent = studentDao.list();
        List<Subjects> listSubjects = subjectsDao.list();
        
        request.setAttribute("listStudent", listStudent);
        request.setAttribute("listSubjects", listSubjects);
        
        MarkDao markDao = new MarkDao();
        Mark mark = markDao.get(id);
        request.setAttribute("item", mark);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/mark/new-mark.jsp");
        dispatcher.forward(request, response);
    }

    private void insert(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        String subjectId = request.getParameter("subjectId");
        String studentId = request.getParameter("studentId");
        Double mark = Double.parseDouble(request.getParameter("mark"));
        
        HttpSession session = request.getSession();
        User userLogin = (User) session.getAttribute("userLogin");
        Mark markAdd = new Mark(subjectId, studentId, mark, userLogin.getId(), new Date(), 0);
        
        MarkDao markDao = new MarkDao();
        boolean result = markDao.insert(markAdd);
        
        if (result == true) {
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Đã có lỗi xảy ra vui lòng kiểm tra lại");
            showNewForm(request, response);
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));
        String subjectId = request.getParameter("subjectId");
        String studentId = request.getParameter("studentId");
        Double mark = Double.parseDouble(request.getParameter("mark"));
        
        HttpSession session = request.getSession();
        User userLogin = (User) session.getAttribute("userLogin");
        Mark markAdd = new Mark(id, subjectId, studentId, mark, id, null, userLogin.getId(),
                new Date(), id, 0, id);
        
        MarkDao markDao = new MarkDao();
        boolean result = markDao.update(markAdd);
        
        if (result == true) {
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Đã có lỗi xảy ra vui lòng kiểm tra lại");
            showEditForm(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        MarkDao markDao = new MarkDao();
        boolean result = markDao.delete(id);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        if (result == true) {
            response.getWriter().write("true");
        } else {
            response.getWriter().write("false");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
