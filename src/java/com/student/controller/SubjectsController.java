/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.controller;

import com.student.dao.GetNextCodeDao;
import com.student.dao.SubjectsDao;
import com.student.model.Student;
import com.student.model.Subjects;
import com.student.model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phuong Ken
 */
public class SubjectsController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getPathInfo().substring(1);
        SubjectsDao subjectsDao = new SubjectsDao();
        try {
            switch (action) {
                case "getForm":
                    showNewForm(request, response);
                    break;
                case "insert":
                    insert(request, response);
                    break;
                case "delete":
                    delete(request, response);
                    break;
                case "edit":
                    showEditForm(request, response);
                    break;
                case "update":
                    update(request, response);
                    break;
                default:
                    List<Subjects> listSubjects = subjectsDao.list();
                    request.setAttribute("listSubjects", listSubjects);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/page/subjects/list-subjects.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        GetNextCodeDao getNextCodeDao = new GetNextCodeDao();
        String subjectCode = getNextCodeDao.getSubjectCode();
        request.setAttribute("subjectCode", subjectCode);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/subjects/new-subjects.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idSubject = Integer.parseInt(request.getParameter("id"));
        SubjectsDao subjectsDao = new SubjectsDao();
        Subjects subjects = subjectsDao.get(idSubject);
        request.setAttribute("item", subjects);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/page/subjects/new-subjects.jsp");
        dispatcher.forward(request, response);
    }

    private void insert(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        String subjectName = request.getParameter("subjectName");
        int status = Integer.parseInt(request.getParameter("status"));
        GetNextCodeDao getNextCodeDao = new GetNextCodeDao();
        String subjectCode = getNextCodeDao.getSubjectCode();
        SubjectsDao subjectsDao = new SubjectsDao();
        
        HttpSession session = request.getSession();
        User userLogin = (User) session.getAttribute("userLogin");

        Subjects subjects = new Subjects(subjectCode, subjectName, userLogin.getId(), new Date(), status);
        boolean result = subjectsDao.insert(subjects);

        if (result == true) {
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Đã có lỗi xảy ra vui lòng kiểm tra lại");
            showNewForm(request, response);
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));
        String subjectName = request.getParameter("subjectName");
        String subjectCode = request.getParameter("subjectCode");
        int status = Integer.parseInt(request.getParameter("status"));
        SubjectsDao subjectsDao = new SubjectsDao();

        HttpSession session = request.getSession();
        User userLogin = (User) session.getAttribute("userLogin");

        
        Subjects subjects = new Subjects(id, subjectCode, subjectName,
                status, null, userLogin.getId(), new Date(), id, status, status);
        
        boolean result = subjectsDao.update(subjects);

        if (result == true) {
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Đã có lỗi xảy ra vui lòng kiểm tra lại");
            showEditForm(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        SubjectsDao subjectsDao = new SubjectsDao();
        boolean result = subjectsDao.delete(id);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        if (result == true) {
            response.getWriter().write("true");
        } else {
            response.getWriter().write("false");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
