/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

import java.util.Date;

/**
 *
 * @author Phuong Ken
 */
public class Mark {
    public int id;
    public String subjectId;
    public String studentId;
    public double mark;
    public int userIdCreated;
    public Date dateCreated;
    public int userIdUpdated;
    public Date dateUpdated;
    public int numberEdit;
    public int status;
    public int deleted;

    public Mark() {
    }

    public Mark(int id, String subjectId, String studentId, double mark, int userIdCreated, Date dateCreated, int userIdUpdated, Date dateUpdated, int numberEdit, int status, int deleted) {
        this.id = id;
        this.subjectId = subjectId;
        this.studentId = studentId;
        this.mark = mark;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.userIdUpdated = userIdUpdated;
        this.dateUpdated = dateUpdated;
        this.numberEdit = numberEdit;
        this.status = status;
        this.deleted = deleted;
    }

    public Mark(String subjectId, String studentId, double mark, int userIdCreated, Date dateCreated, int status) {
        this.subjectId = subjectId;
        this.studentId = studentId;
        this.mark = mark;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.status = status;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    public int getUserIdCreated() {
        return userIdCreated;
    }

    public void setUserIdCreated(int userIdCreated) {
        this.userIdCreated = userIdCreated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getUserIdUpdated() {
        return userIdUpdated;
    }

    public void setUserIdUpdated(int userIdUpdated) {
        this.userIdUpdated = userIdUpdated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getNumberEdit() {
        return numberEdit;
    }

    public void setNumberEdit(int numberEdit) {
        this.numberEdit = numberEdit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    

}
