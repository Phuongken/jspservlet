/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

import java.util.Date;

/**
 *
 * @author Phuong Ken
 */
public class Student {
    public int id;
    public String studentCode;
    public String name;
    public Date dob;
    public int addressId;
    public String addressDetail;
    public long identity;
    public int phoneNumber;
    public int gender;
    public String email;
    public int status;
    public int userIdCreated;
    public Date dateCreated;
    public int userIdUpdated;
    public Date dateUpdated;
    public int numberEdit;
    public int deleted;

    public Student() {
    }

    public Student(int id, String studentCode, String name, Date dob, int addressId, String addressDetail, long identity, int phoneNumber, int gender, String email, int status, int userIdCreated, Date dateCreated, int userIdUpdated, Date dateUpdated, int numberEdit, int deleted) {
        this.id = id;
        this.studentCode = studentCode;
        this.name = name;
        this.dob = dob;
        this.addressId = addressId;
        this.addressDetail = addressDetail;
        this.identity = identity;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.email = email;
        this.status = status;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.userIdUpdated = userIdUpdated;
        this.dateUpdated = dateUpdated;
        this.numberEdit = numberEdit;
        this.deleted = deleted;
    }

    public Student(String studentCode, String name, Date dob, int addressId, String addressDetail, long identity, int phoneNumber, int gender, String email, int status, int userIdCreated, Date dateCreated) {
        this.studentCode = studentCode;
        this.name = name;
        this.dob = dob;
        this.addressId = addressId;
        this.addressDetail = addressDetail;
        this.identity = identity;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.email = email;
        this.status = status;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public long getIdentity() {
        return identity;
    }

    public void setIdentity(long identity) {
        this.identity = identity;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUserIdCreated() {
        return userIdCreated;
    }

    public void setUserIdCreated(int userIdCreated) {
        this.userIdCreated = userIdCreated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getUserIdUpdated() {
        return userIdUpdated;
    }

    public void setUserIdUpdated(int userIdUpdated) {
        this.userIdUpdated = userIdUpdated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getNumberEdit() {
        return numberEdit;
    }

    public void setNumberEdit(int numberEdit) {
        this.numberEdit = numberEdit;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    

}
