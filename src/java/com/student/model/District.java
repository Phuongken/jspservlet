/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

/**
 *
 * @author Phuong Ken
 */
public class District {
    public int id;
    public int provindId;
    public String districtName;
    public String type;
    public int checked;

    public District() {
    }

    public District(int id, int provindId, String districtName, String type, int checked) {
        this.id = id;
        this.provindId = provindId;
        this.districtName = districtName;
        this.type = type;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProvindId() {
        return provindId;
    }

    public void setProvindId(int provindId) {
        this.provindId = provindId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

   
    
}
