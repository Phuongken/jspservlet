/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

/**
 *
 * @author Phuong Ken
 */
public class Provind {
    public int id;
    public String provindName;
    public String type;
    public int checked; 

    public Provind() {
    }

    public Provind(int id, String provindName, String type, int checked) {
        this.id = id;
        this.provindName = provindName;
        this.type = type;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvindName() {
        return provindName;
    }

    public void setProvindName(String provindName) {
        this.provindName = provindName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

   
}
