/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

import java.util.Date;

/**
 *
 * @author Phuong Ken
 */
public class User {

    public int id;
    public String userCode;
    public String userName;
    public String password;
    public String name;
    public Date dob;
    public int addressId;
    public String addressDetail;
    public long identity;
    public int phoneNumber;
    public int gender;
    public String email;
    public int status;
    public int userIdCreated;
    public Date dateCreated;
    public int userIdUpdated;
    public Date dateUpdated;
    public int numberEdit;
    public int deleted;
    public int role;

    public User() {
    }

    public User(int id, String userCode, String userName, String password, String name, Date dob, int addressId, String addressDetail, long identity, int phoneNumber, int gender, String email, int status, int userIdCreated, Date dateCreated, int userIdUpdated, Date dateUpdated, int numberEdit, int deleted, int role) {
        this.id = id;
        this.userCode = userCode;
        this.userName = userName;
        this.password = password;
        this.name = name;
        this.dob = dob;
        this.addressId = addressId;
        this.addressDetail = addressDetail;
        this.identity = identity;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.email = email;
        this.status = status;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.userIdUpdated = userIdUpdated;
        this.dateUpdated = dateUpdated;
        this.numberEdit = numberEdit;
        this.deleted = deleted;
        this.role = role;
    }

    public User(String userCode, String userName, String password, String name, Date dob, int addressId, String addressDetail, long identity, int phoneNumber, int gender, String email, int status, int userIdCreated, Date dateCreated, int role) {
        this.userCode = userCode;
        this.userName = userName;
        this.password = password;
        this.name = name;
        this.dob = dob;
        this.addressId = addressId;
        this.addressDetail = addressDetail;
        this.identity = identity;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.email = email;
        this.status = status;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.role = role;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public long getIdentity() {
        return identity;
    }

    public void setIdentity(long identity) {
        this.identity = identity;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUserIdCreated() {
        return userIdCreated;
    }

    public void setUserIdCreated(int userIdCreated) {
        this.userIdCreated = userIdCreated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getUserIdUpdated() {
        return userIdUpdated;
    }

    public void setUserIdUpdated(int userIdUpdated) {
        this.userIdUpdated = userIdUpdated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getNumberEdit() {
        return numberEdit;
    }

    public void setNumberEdit(int numberEdit) {
        this.numberEdit = numberEdit;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

   

}
