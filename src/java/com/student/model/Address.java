/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

/**
 *
 * @author Phuong Ken
 */
public class Address {
    public int id;
    public int provindId;
    public int districtId;
    public int townId;
    public String provindName;
    public String districtName;
    public String townName;

    public Address() {
    }

    public Address(int id, int provindId, int districtId, int townId) {
        this.id = id;
        this.provindId = provindId;
        this.districtId = districtId;
        this.townId = townId;
    }

    public Address(int id, int provindId, int districtId, int townId, String provindName, String districtName, String townName) {
        this.id = id;
        this.provindId = provindId;
        this.districtId = districtId;
        this.townId = townId;
        this.provindName = provindName;
        this.districtName = districtName;
        this.townName = townName;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProvindId() {
        return provindId;
    }

    public void setProvindId(int provindId) {
        this.provindId = provindId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getTownId() {
        return townId;
    }

    public void setTownId(int townId) {
        this.townId = townId;
    }

    public String getProvindName() {
        return provindName;
    }

    public void setProvindName(String provindName) {
        this.provindName = provindName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }
    
    
}
