/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

import java.util.Date;

/**
 *
 * @author Phuong Ken
 */
public class Subjects {
    public int id;
    public String subjectCode;
    public String subjectName;
    public int userIdCreated;
    public Date dateCreated;
    public int userIdUpdated;
    public Date dateUpdated;
    public int numberEdit;
    public int status;
    public int deleted;

    public Subjects() {
    }

    public Subjects(int id, String subjectCode, String subjectName, int userIdCreated, Date dateCreated, int userIdUpdated, Date dateUpdated, int numberEdit, int status, int deleted) {
        this.id = id;
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.userIdUpdated = userIdUpdated;
        this.dateUpdated = dateUpdated;
        this.numberEdit = numberEdit;
        this.status = status;
        this.deleted = deleted;
    }

    public Subjects(String subjectCode, String subjectName, int userIdCreated, Date dateCreated, int status) {
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.userIdCreated = userIdCreated;
        this.dateCreated = dateCreated;
        this.status = status;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

     public int getUserIdCreated() {
        return userIdCreated;
    }

    public void setUserIdCreated(int userIdCreated) {
        this.userIdCreated = userIdCreated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getUserIdUpdated() {
        return userIdUpdated;
    }

    public void setUserIdUpdated(int userIdUpdated) {
        this.userIdUpdated = userIdUpdated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }
    
    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getNumberEdit() {
        return numberEdit;
    }

    public void setNumberEdit(int numberEdit) {
        this.numberEdit = numberEdit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

}
