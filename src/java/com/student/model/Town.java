/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.model;

/**
 *
 * @author Phuong Ken
 */
public class Town {
    public int id;
    public int districtId;
    public String townName;
    public String type;
    public int checked;

    public Town() {
    }

    public Town(int id, int districtId, String townName, String type, int checked) {
        this.id = id;
        this.districtId = districtId;
        this.townName = townName;
        this.type = type;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

   
   
}
