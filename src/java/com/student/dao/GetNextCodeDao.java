/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Phuong Ken
 */
public class GetNextCodeDao {
    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }
    
    public String getUserCode() throws SQLException{
            Connection connection = getConnection();

            String sql = "SELECT COUNT(id) AS count FROM user";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();
            int count = 0;
            if (resultSet.next()) {
                count = Integer.parseInt(resultSet.getString("count")) + 10000;
            }
            String user = "USER" + count;
        return user;
    }
    
    public String getStudentCode() throws SQLException{
            Connection connection = getConnection();

            String sql = "SELECT COUNT(id) AS count FROM student";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();
            int count = 0;
            if (resultSet.next()) {
                count = Integer.parseInt(resultSet.getString("count")) + 10000;
            }
            String student = "Student" + count;
        return student;
    }
    
    public String getSubjectCode() throws SQLException{
            Connection connection = getConnection();

            String sql = "SELECT COUNT(id) AS count FROM subjects";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();
            int count = 0;
            if (resultSet.next()) {
                count = Integer.parseInt(resultSet.getString("count")) + 10000;
            }
            String subject = "Subject" + count;
        return subject;
    }
}
