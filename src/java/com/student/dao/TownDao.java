/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.Subjects;
import com.student.model.Town;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class TownDao {
    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(Town town) {
        try {
            Connection connection = getConnection();
            
            String sql = "INSERT INTO town (dictrictId,townName,type,checked) VALUES (?,?,?,?)";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, town.getDistrictId());
            pre.setString(2, town.getTownName());
            pre.setString(3, town.getType());
            pre.setInt(4, town.getChecked());
             boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
 
     public boolean update(Town town) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE town SET dictrictId = ?, townName = ?, type= ? WHERE id = ?";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, town.getDistrictId());
            pre.setString(2, town.getTownName());
            pre.setString(3, town.getType());
            pre.setInt(4, town.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public Town get(int idSubject) {
        try {
            Connection connection = getConnection();

            Town town = null;
            String sql = "SELECT * FROM town Where id = ?";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idSubject);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                int dictrictId = resultSet.getInt("districtId");
                String townName = resultSet.getString("townName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
                town = new Town(id, dictrictId, townName, type, checked);
            }
            return town;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Town> list(){
        try {
            Connection connection = getConnection();
            List<Town> listTown = new ArrayList<>();

            String sql = "SELECT * FROM town";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int dictrictId = resultSet.getInt("districtId");
                String townName = resultSet.getString("townName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
               Town town = new Town(id, dictrictId, townName, type, checked);
               listTown.add(town);
            }
            return listTown;
        } catch (Exception ex) {
            return null;
        }
    }
    
    public List<Town> getListByDistrictID(int idDistrict){
    try {
            Connection connection = getConnection();
            List<Town> listTown = new ArrayList<>();

            String sql = "SELECT * FROM town where districtId = ?";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idDistrict);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int dictrictId = resultSet.getInt("districtId");
                String townName = resultSet.getString("townName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
               Town town = new Town(id, dictrictId, townName, type, checked);
               listTown.add(town);
            }
            return listTown;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "DELETE town WHERE id = ? AND checked = 1";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
