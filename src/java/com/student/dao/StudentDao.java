/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.Student;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class StudentDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(Student student) {
        try {
            Connection connection = getConnection();

            String sql = "INSERT INTO student (studentCode,name,dob,addressId,addressDetail,"
                    + "identity,phoneNumber,gender,email,status, userIdCreated,dateCreated"
                    + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

            java.sql.Date dob = new java.sql.Date(student.getDob().getTime());
            java.sql.Date datecreated = new java.sql.Date(student.getDateCreated().getTime());

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, student.getStudentCode());
            pre.setString(2, student.getName());
            pre.setDate(3, dob);
            pre.setInt(4, student.getAddressId());
            pre.setString(5, student.getAddressDetail());
            pre.setLong(6, student.getIdentity());
            pre.setInt(7, student.getPhoneNumber());
            pre.setInt(8, student.getGender());
            pre.setString(9, student.getEmail());
            pre.setInt(10, student.getStatus());
            pre.setInt(11, student.getUserIdCreated());
            pre.setDate(12, datecreated);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean update(Student student) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE student SET name = ?,dob = ?, addressId = ? ,addressDetail = ?,"
                    + "identity = ?, phoneNumber = ?,gender = ?,email =?,status = ?, userIdUpdated = ?,"
                    + "dateUpdated = ?,numberEdit = ? WHERE id = ?";

            java.sql.Date dob = new java.sql.Date(student.getDob().getTime());
            java.sql.Date dateupdated = new java.sql.Date(student.getDateUpdated().getTime());

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, student.getName());
            pre.setDate(2, dob);
            pre.setInt(3, student.getAddressId());
            pre.setString(4, student.getAddressDetail());
            pre.setLong(5, student.getIdentity());
            pre.setInt(6, student.getPhoneNumber());
            pre.setInt(7, student.getGender());
            pre.setString(8, student.getEmail());
            pre.setInt(9, student.getStatus());
            pre.setInt(10, student.getUserIdUpdated());
            pre.setDate(11, dateupdated);
            pre.setInt(12, student.getNumberEdit());
            pre.setInt(13, student.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public Student get(int idStudent) {
        try {
            Connection connection = getConnection();

            Student student = null;
            String sql = "SELECT * FROM student WHERE id = ? AND deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idStudent);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String studentCode = resultSet.getString("studentCode");
                String name = resultSet.getString("name");
                Date dob = resultSet.getDate("dob");
                int addressId = resultSet.getInt("addressId");
                String addressDetail = resultSet.getString("addressDetail");
                int identity = resultSet.getInt("identity");
                int phoneNumber = resultSet.getInt("phoneNumber");
                int gender = resultSet.getInt("gender");
                String email = resultSet.getString("email");
                int status = resultSet.getInt("status");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int deleted = resultSet.getInt("deleted");

                student = new Student(id, studentCode, name, dob,
                        addressId, addressDetail, identity, phoneNumber,
                        gender, email, status, userIdCreated, dateCreated,
                        userIdUpdated, dateUpdated, numberEdit, deleted);
            }
            return student;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Student> list() throws SQLException {
        try {
            Connection connection = getConnection();
            List<Student> listStudent = new ArrayList<>();

            String sql = "SELECT * FROM student WHERE deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String studentCode = resultSet.getString("studentCode");
                String name = resultSet.getString("name");
                Date dob = resultSet.getDate("dob");
                int addressId = resultSet.getInt("addressId");
                String addressDetail = resultSet.getString("addressDetail");
                int identity = resultSet.getInt("identity");
                int phoneNumber = resultSet.getInt("phoneNumber");
                int gender = resultSet.getInt("gender");
                String email = resultSet.getString("email");
                int status = resultSet.getInt("status");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int deleted = resultSet.getInt("deleted");

                Student student = new Student(id, studentCode, name, dob, addressId,
                        addressDetail, identity, phoneNumber,
                        gender, email, status, userIdCreated,
                        dateCreated, userIdUpdated, dateUpdated, numberEdit, deleted);
                listStudent.add(student);
            }
            return listStudent;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "UPDATE student SET deleted = 1 WHERE id = ? AND deleted = 0";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
