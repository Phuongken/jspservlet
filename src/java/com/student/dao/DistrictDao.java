/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.District;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class DistrictDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(District district) {
        try {
            Connection connection = getConnection();

            String sql = "INSERT INTO provind (provindId,districtName,type,checked) VALUES (?,?,?,?)";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, district.getProvindId());
            pre.setString(2, district.getDistrictName());
            pre.setString(3, district.getType());
            pre.setInt(4, district.getChecked());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean update(District district) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE provind SET provindId = ?,districtName= ?,type = ? WHERE id = ?";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, district.getProvindId());
            pre.setString(2, district.getDistrictName());
            pre.setString(3, district.getType());
            pre.setInt(4, district.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public District get(int idDistrict) {
        try {
            Connection connection = getConnection();

            District district = null;
            String sql = "SELECT * FROM district Where id = ?";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idDistrict);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                int provindId = resultSet.getInt("provindId");
                String dictrictName = resultSet.getString("districtName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
                district = new District(id, provindId, dictrictName, type, checked);
            }
            return district;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<District> list() {
        List<District> listDistrict = new ArrayList<>();
        String sql = "SELECT * FROM district";

        try (Connection connection = getConnection();
                PreparedStatement pre = connection.prepareStatement(sql);) {

            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int provindId = resultSet.getInt("provindId");
                String districtName = resultSet.getString("districtName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
                listDistrict.add(new District(id, provindId, districtName, type, checked));
            }
        } catch (Exception ex) {
            return null;
        }
        return listDistrict;
    }
    
    public List<District> getListByProvindId(int idProvind) {
        List<District> listDistrict = new ArrayList<>();
        String sql = "SELECT * FROM district WHERE provindId = ?";

        try (Connection connection = getConnection();
                PreparedStatement pre = connection.prepareStatement(sql);
                ) {
            pre.setInt(1, idProvind);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int provindId = resultSet.getInt("provindId");
                String districtName = resultSet.getString("districtName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
                listDistrict.add(new District(id, provindId, districtName, type, checked));
            }
        } catch (Exception ex) {
            return null;
        }
        return listDistrict;
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "DELETE district WHERE id = ? AND checked = 1";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
