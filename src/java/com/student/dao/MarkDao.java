/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.Mark;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class MarkDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(Mark mark) {
        try {
            Connection connection = getConnection();

            String sql = "INSERT INTO mark (subjectId, studentId,mark,userIdCreated,dateCreated"
                    + ",status) VALUES (?,?,?,?,?,?)";
            
             java.sql.Date date = new java.sql.Date(mark.getDateCreated().getTime());

             PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, mark.getSubjectId());
            pre.setString(2, mark.getStudentId());
            pre.setDouble(3, mark.getMark());
            pre.setInt(4, mark.getUserIdCreated());
            pre.setDate(5, date);
            pre.setInt(6, mark.getStatus());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean update(Mark mark) {
        try {

            Connection connection = getConnection();
            String sql = "UPDATE mark SET mark = ?, userIdUpdated = ?, dateUpdated = ?, "
                    + "numberEdit = ?, status = ? WHERE id = ?";
            
            java.sql.Date date = new java.sql.Date(mark.getDateUpdated().getTime());

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setDouble(1, mark.getMark());
            pre.setInt(2, mark.getUserIdUpdated());
            pre.setDate(3, date);
            pre.setInt(4, mark.getNumberEdit());
            pre.setInt(5, mark.getStatus());
            pre.setInt(6, mark.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public Mark get(int idMark) {
        try {
            Connection connection = getConnection();

            Mark markResult = null;
            String sql = "SELECT * FROM mark WHERE id = ? AND deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idMark);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String subjectId = resultSet.getString("subjectId");
                String studentId = resultSet.getString("studentId");
                double mark = resultSet.getDouble("mark");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int status = resultSet.getInt("status");
                int deleted = resultSet.getInt("deleted");
                markResult = new Mark(id, subjectId, studentId, mark, userIdCreated, dateCreated,
                        userIdUpdated, dateUpdated, numberEdit, status, deleted);
            }
            return markResult;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Mark> list() throws SQLException {
        try {
            Connection connection = getConnection();
            List<Mark> listMark = new ArrayList<>();

            String sql = "SELECT * FROM mark WHERE deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String subjectId = resultSet.getString("subjectId");
                String studentId = resultSet.getString("studentId");
                double mark = resultSet.getDouble("mark");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int status = resultSet.getInt("status");
                int deleted = resultSet.getInt("deleted");
                Mark markResult = new Mark(id, subjectId, studentId, mark, userIdCreated, dateCreated, userIdUpdated, dateUpdated, numberEdit, status, deleted);
                listMark.add(markResult);
            }
            return listMark;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "UPDATE mark SET deleted = 1 WHERE id = ? AND deleted = 0";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
