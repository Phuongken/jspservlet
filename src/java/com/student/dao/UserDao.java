/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.User;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class UserDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(User user) {
        try {
            
            boolean checkAccount = checkAccount(user.getUserName());
            if(checkAccount == true) return false;
            Encryptor encryptor = new Encryptor();

            Connection connection = getConnection();

            
            String sql = "INSERT INTO user (userCode, userName, password,name,dob,addressId,addressDetail,"
                    + "identity,phoneNumber,gender,email,status, userIdCreated,dateCreated"
                    + ",role) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            java.sql.Date dob = new java.sql.Date(user.getDob().getTime());
            java.sql.Date datecreated = new java.sql.Date(user.getDateCreated().getTime());

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, user.getUserCode());
            pre.setString(2, user.getUserName());
            pre.setString(3, encryptor.getMD5(user.getPassword()));
            pre.setString(4, user.getName());
            pre.setDate(5, dob);
            pre.setInt(6, user.getAddressId());
            pre.setString(7, user.getAddressDetail());
            pre.setLong(8, user.getIdentity());
            pre.setInt(9, user.getPhoneNumber());
            pre.setInt(10, user.getGender());
            pre.setString(11, user.getEmail());
            pre.setInt(12, user.getStatus());
            pre.setInt(13, user.getUserIdCreated());
            pre.setDate(14, datecreated);
            pre.setInt(15, user.getRole());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean checkAccount(String userName) throws SQLException {
         Connection connection = getConnection();

        String check = "SELECT * FROM user WHERE userName LIKE " + userName;

        PreparedStatement preCheck = connection.prepareStatement(check);
        ResultSet resultSet = preCheck.executeQuery();
        if(resultSet.next()){
            return true;
        } 
        return false;
    }

    public boolean update(User user) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE user SET name = ?,dob = ?, addressId = ? ,addressDetail = ?,"
                    + "identity = ?, phoneNumber = ?,gender = ?,email =?,status = ?, userIdUpdated = ?,"
                    + "dateUpdated = ?,numberEdit = ?, role = ? WHERE id = ?";

            java.sql.Date dob = new java.sql.Date(user.getDob().getTime());
            java.sql.Date dateupdated = new java.sql.Date(user.getDateUpdated().getTime());

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, user.getName());
            pre.setDate(2, dob);
            pre.setInt(3, user.getAddressId());
            pre.setString(4, user.getAddressDetail());
            pre.setLong(5, user.getIdentity());
            pre.setInt(6, user.getPhoneNumber());
            pre.setInt(7, user.getGender());
            pre.setString(8, user.getEmail());
            pre.setInt(9, user.getStatus());
            pre.setInt(10, user.getUserIdUpdated());
            pre.setDate(11, dateupdated);
            pre.setInt(12, user.getNumberEdit());
            pre.setInt(13, user.getRole());
            pre.setInt(14, user.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public User get(int idUser) {
        try {
            Connection connection = getConnection();

            User user = null;
            String sql = "SELECT * FROM user WHERE id = ? AND deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idUser);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String userCode = resultSet.getString("userCode");
                String userName = resultSet.getString("userName");
                String password = resultSet.getString("password");
                String name = resultSet.getString("name");
                Date dob = resultSet.getDate("dob");
                int addressId = resultSet.getInt("addressId");
                String addressDetail = resultSet.getString("addressDetail");
                int identity = resultSet.getInt("identity");
                int phoneNumber = resultSet.getInt("phoneNumber");
                int gender = resultSet.getInt("phoneNumber");
                String email = resultSet.getString("email");
                int status = resultSet.getInt("status");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int role = resultSet.getInt("role");
                int deleted = resultSet.getInt("deleted");

                user = new User(id, userCode, userName, password, name, dob, addressId, addressDetail, identity, phoneNumber, gender, email, status, userIdCreated, dateCreated, userIdUpdated, dateUpdated, numberEdit, deleted, role);
            }
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<User> list() throws SQLException {
        try {
            Connection connection = getConnection();
            List<User> listUser = new ArrayList<>();

            String sql = "SELECT * FROM user WHERE deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String userCode = resultSet.getString("userCode");
                String userName = resultSet.getString("userName");
                String password = resultSet.getString("password");
                String name = resultSet.getString("name");
                Date dob = resultSet.getDate("dob");
                int addressId = resultSet.getInt("addressId");
                String addressDetail = resultSet.getString("addressDetail");
                int identity = resultSet.getInt("identity");
                int phoneNumber = resultSet.getInt("phoneNumber");
                int gender = resultSet.getInt("phoneNumber");
                String email = resultSet.getString("email");
                int status = resultSet.getInt("status");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int role = resultSet.getInt("role");
                int deleted = resultSet.getInt("deleted");

                User user = new User(id, userCode, userName, password, name, dob, addressId, addressDetail, identity, phoneNumber, gender, email, status, userIdCreated, dateCreated, userIdUpdated, dateUpdated, numberEdit, deleted, role);
                listUser.add(user);
            }
            return listUser;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "UPDATE user SET deleted = 1 WHERE id = ? AND deleted = 0";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
