/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.Subjects;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class SubjectsDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(Subjects subjects) {
        try {
            Connection connection = getConnection();

            String sql = "INSERT INTO subjects "
                    + "(subjectCode,subjectName,userIdCreated,dateCreated,status)"
                    + " VALUES (?,?,?,?,?)";
            
            java.sql.Date date = new java.sql.Date(subjects.getDateCreated().getTime());
            
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, subjects.getSubjectCode());
            pre.setString(2, subjects.getSubjectName());
            pre.setInt(3, subjects.getUserIdCreated());
            pre.setDate(4, date);
            pre.setInt(5, subjects.getStatus());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean update(Subjects subjects) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE subjects SET subjectName = ?, userIdUpdated = ?,"
                    + " dateUpdated = ?, numberEdit = ?, status = ? WHERE id = ?";
            
            java.sql.Date date = new java.sql.Date(subjects.getDateUpdated().getTime());
            
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, subjects.getSubjectName());
            pre.setInt(2, subjects.getUserIdUpdated());
            pre.setDate(3, date);
            pre.setInt(4, subjects.getNumberEdit());
            pre.setInt(5, subjects.getStatus());
            pre.setInt(6, subjects.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public Subjects get(int idSubject) {
        try {
            Connection connection = getConnection();

            Subjects subject = null;
            String sql = "SELECT * FROM subjects Where id = ? AND deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idSubject);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String subjectCode = resultSet.getString("subjectCode");
                String subjectName = resultSet.getString("subjectName");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int status = resultSet.getInt("status");
                int deleted = resultSet.getInt("deleted");
                subject = new Subjects(id, subjectCode, subjectName, userIdCreated,
                        dateCreated, userIdUpdated, dateUpdated, numberEdit, status, deleted);
            }
            return subject;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Subjects> list() throws SQLException {
        try {
            Connection connection = getConnection();
            List<Subjects> listSubject = new ArrayList<>();

            String sql = "SELECT * FROM subjects WHERE deleted = 0";

            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String subjectCode = resultSet.getString("subjectCode");
                String subjectName = resultSet.getString("subjectName");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int status = resultSet.getInt("status");
                int deleted = resultSet.getInt("deleted");
                Subjects subject = new Subjects(id, subjectCode, subjectName,
                        userIdCreated, dateCreated, userIdUpdated,
                        dateUpdated, numberEdit, status, deleted);
                listSubject.add(subject);
            }
            return listSubject;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "UPDATE subjects SET deleted = 1 WHERE id = ? AND deleted = 0";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
