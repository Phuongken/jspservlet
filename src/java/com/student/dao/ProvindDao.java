/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.Provind;
import com.student.model.Subjects;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class ProvindDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public boolean insert(Provind provind) {
        try {
            Connection connection = getConnection();

            String sql = "INSERT INTO provind (provindName,type,checked) VALUES (?,?,?)";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, provind.getProvindName());
            pre.setString(2, provind.getType());
            pre.setInt(3, provind.getChecked());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean update(Provind provind) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE provind SET provindName = ?, type = ? WHERE id = ?";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, provind.getProvindName());
            pre.setString(2, provind.getType());
            pre.setInt(3, provind.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public Provind get(int idProvind) {
        try {
            Connection connection = getConnection();

            Provind provind = null;
            String sql = "SELECT * FROM provind Where id = ?";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idProvind);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String provindName = resultSet.getString("provindName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
                provind = new Provind(id, provindName, type, checked);
            }
            return provind;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Provind> list() {
        List<Provind> listProvind = new ArrayList<>();
        String sql = "SELECT * FROM provind";

        try (Connection connection = getConnection();
                PreparedStatement pre = connection.prepareStatement(sql);) {

            ResultSet resultSet = pre.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String provindName = resultSet.getString("provindName");
                String type = resultSet.getString("type");
                int checked = resultSet.getInt("checked");
                listProvind.add(new Provind(id, provindName, type, checked));
            }
        } catch (Exception ex) {
            return null;
        }
        return listProvind;
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "DELETE provind WHERE id = ? AND checked = 1";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
