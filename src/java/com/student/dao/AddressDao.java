/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.Address;
import com.student.model.Subjects;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phuong Ken
 */
public class AddressDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public int insert(Address address) {
        try {
            Connection connection = getConnection();
            String generatedColumns[] = {"ID"};
            String sql = "INSERT INTO address (provindId,districtId,townId) VALUES (?,?,?)";
            PreparedStatement pre = connection.prepareStatement(sql, generatedColumns);
            pre.setInt(1, address.getProvindId());
            pre.setInt(2, address.getDistrictId());
            pre.setInt(3, address.getTownId());
            int rowInserted = pre.executeUpdate();
            if (rowInserted > 0) {
                try (ResultSet generatedKeys = pre.getGeneratedKeys()) {
                    int id = 0;
                    if (generatedKeys.next()) {
                        id = generatedKeys.getInt(1);
                    }
                    return id;
                }
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean update(Address address) {
        try {
            Connection connection = getConnection();

            String sql = "UPDATE address SET provindId = ?, districtId =?, townId = ?  WHERE id = ?";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, address.getProvindId());
            pre.setInt(2, address.getDistrictId());
            pre.setInt(3, address.getTownId());
            pre.setInt(4, address.getId());
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception ex) {
            return false;
        }
    }

    public Address get(int idSubject) {
        try {
            Connection connection = getConnection();

            Address address = null;
            String sql = "SELECT a.*, p.provindName, d.districtName, t.townName FROM address AS a  LEFT JOIN provind AS p ON p.id = a.provindId"
                    + " LEFT JOIN district AS d ON d.id = a.districtId LEFT JOIN town AS t ON t.id = a.townId WHERE a.id = ?";

            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, idSubject);
            ResultSet resultSet = pre.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                int provindId = resultSet.getInt("provindId");
                int districtId = resultSet.getInt("districtId");
                int townId = resultSet.getInt("townId");
                String provindName = resultSet.getString("provindName");
                String districtName = resultSet.getString("districtName");
                String townName = resultSet.getString("townName");
                address = new Address(id, provindId, districtId, townId, 
                        provindName, districtName, townName);
            }
            return address;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = getConnection();
            String sql = "DELETE adress WHERE id = ?";
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            boolean rowInserted = pre.executeUpdate() > 0;
            return rowInserted;
        } catch (Exception e) {
            return false;
        }
    }
}
