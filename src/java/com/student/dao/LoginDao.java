/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student.dao;

import com.student.model.User;
import com.student.ultil.DbConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Phuong Ken
 */
public class LoginDao {

    protected Connection getConnection() {
        DbConnection db = new DbConnection();
        return db.getConnection();
    }

    public User Login(String userName, String pasword) {
        try {
            Connection connection = getConnection();

            Encryptor encryptor = new Encryptor();
            pasword = encryptor.getMD5(pasword);
            String sql = "SELECT * FROM user WHERE userName LIKE " +"'" + userName +"'"+ " AND password LIKE "+"'" + pasword +"'";
            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet resultSet = pre.executeQuery();
            
            User user = null;
            
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String userCode = resultSet.getString("userCode");
                String name = resultSet.getString("name");
                Date dob = resultSet.getDate("dob");
                int addressId = resultSet.getInt("addressId");
                String addressDetail = resultSet.getString("addressDetail");
                int identity = resultSet.getInt("identity");
                int phoneNumber = resultSet.getInt("phoneNumber");
                int gender = resultSet.getInt("phoneNumber");
                String email = resultSet.getString("email");
                int status = resultSet.getInt("status");
                int userIdCreated = resultSet.getInt("userIdCreated");
                Date dateCreated = resultSet.getDate("dateCreated");
                int userIdUpdated = resultSet.getInt("userIdUpdated");
                Date dateUpdated = resultSet.getDate("dateUpdated");
                int numberEdit = resultSet.getInt("numberEdit");
                int deleted = resultSet.getInt("deleted");
                
                user = new User(id, userCode, userName, pasword, name, dob,
                        addressId, addressDetail, identity, 
                        phoneNumber, gender, email, status, 
                        userIdCreated, dateCreated, userIdUpdated, dateUpdated, numberEdit, deleted, id);
               
            }
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

}
