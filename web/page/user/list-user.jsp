<%-- 
    Document   : list-user
    Created on : May 28, 2019, 9:37:38 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <a href="getForm" class="btn btn-success">Thêm mới</a>
                </div>
            </div>
            <div class="row margin-top-10">
                <div class="col-lg-12">
                    <table class="table">
                        <thead class="tableClass">
                            <tr>
                                <th>Mã tài khoản</th>
                                <th>Tên</th>
                                <th>Giới tính</th>
                                <th>Ngày sinh</th>
                                <th>Số điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${listUser}">
                                <tr>
                                    <td><c:out value="${item.id}"/></td>
                                    <td><c:out value="${item.name}"/></td>
                                    <td><c:out value="${item.gender}"/></td>
                                    <td><c:out value="${item.dob}"/></td>
                                    <td><c:out value="${item.phoneNumber}"/></td>
                                    <td><c:out value="${item.addressDetail}"/></td>
                                    <td>
                                        <a href="edit?id=<c:out value='${item.id}'/>">Sửa</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#" onclick="deleteUser(${item.id})">Xóa</a>                     
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </jsp:attribute>
</templace:template>