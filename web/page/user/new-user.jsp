<%-- 
    Document   : new-user
    Created on : May 28, 2019, 9:37:52 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <h4>Thêm mới tài khoản</h4>
                </div>
            </div>
            <c:if test="${item.id != null}">
                <form action="update" method="post">
                </c:if>
                <c:if test="${item.id == null}">
                    <form action="insert" method="post">
                    </c:if>
                    <c:if test="${error != null}">
                        <div class="row">
                            <div class="col-lg-12">
                                <h6 style="color: red">${error}</h5>
                            </div>
                        </div>
                    </c:if>
                    <div>
                        <input type="hidden" name="id"  value="<c:out value='${item.id}'/>"  />
                        <input type="hidden" name="addressId"  value="<c:out value='${item.addressId}'/>"/>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Mã tài khoản">Mã TK <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <c:if test="${item.userCode == null}">
                                        <input disabled="true" type="text" name="userCode" class="form-control" value="<c:out value='${userCode}'/>" />
                                    </c:if>
                                    <c:if test="${item.userCode != null}">
                                        <input disabled="true" type="text" value="<c:out value='${item.userCode}'/>" name="userCode" class="form-control"  />
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Tên">Tên <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="name" class="form-control" value="<c:out value='${item.name}'/>"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Tên sinh viên">Ngày sinh <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="date" name="dob" class="form-control" value="<c:out value='${item.dob}'/>"  />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Tỉnh-Thành">Tỉnh-Thành <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="provindId" id="provindId" onchange="provindChange()" class="form-control"/>" >
                                    <c:if test="${itemAddress.provindId == null}">
                                        <option value="-1">Chọn Tỉnh-Thành</option>
                                    </c:if>
                                    <c:if test="${itemAddress.provindId != null}">
                                        <option value="${itemAddress.provindId}">${itemAddress.provindName}</option>
                                    </c:if>
                                    <c:forEach items="${listProvind}" var="provind">
                                        <option value="${provind.id}">${provind.provindName}</option>
                                    </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Quận-Huyện">Quận-Huyện <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="districtId" id="districtId" onchange="districtChange()" class="form-control" >
                                        <c:if test="${itemAddress.districtId == null}">
                                            <option value="-1">Chọn Quận-Huyện</option>
                                        </c:if>
                                        <c:if test="${itemAddress.districtId != null}">
                                            <option value="${itemAddress.districtId}">${itemAddress.districtName}</option>
                                        </c:if>
                                        <c:forEach items="${listDistrict}" var="district">
                                            <option value="${district.id}">${district.districtName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Xã-Phuờng">Xã-Phuờng <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="townId" class="form-control" id="townId">
                                        <c:if test="${itemAddress.townId == null}">
                                            <option value="-1">Chọn Quận-Huyện</option>
                                        </c:if>
                                        <c:if test="${itemAddress.townId != null}">
                                            <option value="${itemAddress.townId}">${itemAddress.townName}</option>
                                        </c:if>
                                        <c:forEach items="${listTown}" var="town">
                                            <option value="${town.id}">${town.townName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Địa chỉ chi tiết">Địa chỉ CT <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="addressDetail" class="form-control" value="<c:out value='${item.addressDetail}'/>"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Chứng minh thư căn cước công dân">CM.Thư- CCCD <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="identity" class="form-control" value="<c:out value='${item.identity}'/>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Số điện thoại">Số điện thoại <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="phoneNumber" value="<c:out value='${item.phoneNumber}'/>" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Giới tính">Giới tính <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="gender" class="form-control">
                                        <c:if test="${item.gender == 0}">
                                            <option value="0">Nam</option>
                                        </c:if>
                                        <c:if test="${item.gender == 1}">
                                            <option value="1">Nữ</option>
                                        </c:if>
                                        <option value="0">Nam</option>
                                        <option value="1">Nữ</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Email">Email <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="email" class="form-control" value="<c:out value='${item.email}'/>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 margin-top-10">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Trạng thái">Trạng thái <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="status" class="form-control">
                                        <c:if test="${item.status == 0}">
                                            <option value="0">Hoạt động</option>
                                        </c:if>
                                        <c:if test="${item.status == 1}">
                                            <option value="1">Ngừng hoạt động</option>
                                        </c:if>
                                        <option value="0">Hoạt động</option>
                                        <option value="1">Ngừng hoạt động</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <c:if test="${item.id == null}">
                        <div class="row">
                            <div class="col-lg-4 margin-top-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label class="control-lable" title="Tài khoản">Tài khoản <span class="require">*</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="text" name="userName" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 margin-top-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label class="control-lable" title="Mật khẩu">Mật khẩu <span class="require">*</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="password" name="password" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 margin-top-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label class="control-lable" title="Xác nhận mật khẩu">Xác nhận mật khẩu <span class="require">*</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="password" name="confirmPassword" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 margin-top-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label class="control-lable" title="Phân quyền">Phân quyền <span class="require">*</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                        <c:if test="${item.role == 0}">
                                            <option value="0">Admin</option>
                                        </c:if>
                                        <c:if test="${item.role == 1}">
                                            <option value="1">Xem</option>
                                        </c:if>
                                        <option value="0">Admin</option>
                                        <option value="1">Xem</option>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <div class="row margin-top-10">
                        <div class="col-lg-12 text-right">
                            <button type="submit" class="btn btn-success">Lưu</button>
                        </div>
                    </div>
                </form>
        </div>
    </jsp:attribute>
</templace:template>