<%-- 
    Document   : register
    Created on : May 26, 2019, 8:38:27 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Đăng kí</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h2 class="text-center">Đăng ký tài khoản</h2   >
            <form action="" method="post" class="p-2" style="border: 1px solid #bdbdbd; border-radius: 3px;">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12 border-right">
                        <div class="row mb-4">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <h4>Thông tin cá nhân</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">Tên</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="text" class="form-control" name="name"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">Ngày sinh</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="date" class="form-control" name="dob"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">Tỉnh-Thành</label>
                                <select name="provindId" id="provindId" onchange="provindChange()" class="form-control">
                                    <option value="-1">Chọn Tỉnh-Thành</option>
                                    <c:forEach items="${listProvind}" var="provind">
                                        <option value="${provind.id}">${provind.provindName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">Quận-Huyện</label>
                                <select name="districtId" id="districtId" onchange="districtChange()" class="form-control">
                                    <option value="-1">Chọn Quận-Huyện</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">Xã Phường</label>
                                <select name="townId" class="form-control" id="townId">
                                   <option value="-1">Chọn Xã-Phường</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">Địa chỉ chi tiết</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="text" class="form-control" name="addressDetail"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable" title="Chứng minh thư căn cước công dân">
                                    CM.Thư - CC.Công dân
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="text" class="form-control" name="identity"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">
                                    Số điện thoại
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="text" class="form-control" name="phoneNumber"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">
                                    Giới tính
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <select name="gender" class="form-control">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">
                                    Email
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="text" class="form-control" name="email"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="row mb-4">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <h4>Thông tin tài khoản</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">
                                    Tài khoản
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="text" class="form-control" name="userName"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">
                                    Mật khẩu
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="password" class="form-control" name="password"/>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-12">
                                <label class="control-lable">
                                    Xác nhận mật khẩu
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <input type="password" class="form-control" name="confirmpassword"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-success">Đăng ký</button>
                        <button class="btn btn-dark">Làm lại</button>
                    </div>
                </div>
            </form>
        </div>
        <script src="../ManagerStudent/assets/js/address.js" type="text/javascript"></script>
    </body>
</html>
