<%-- 
    Document   : list-subject
    Created on : May 28, 2019, 9:36:41 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <a href="getForm" class="btn btn-success">Thêm mới</a>
                </div>
            </div>
            <div class="row margin-top-10">
                <div class="col-lg-12">
                    <table class="table">
                        <thead class="tableClass">
                            <tr>
                                <th>Mã môn học</th>
                                <th>Tên môn học</th>
                                <th>Trạng thái</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${listSubjects}">
                                <tr>
                                    <td><c:out value="${item.subjectCode}"/></td>
                                    <td><c:out value="${item.subjectName}"/></td>
                                    <td>
                                        <c:if test="${item.status == 0}">
                                            Đang sử dụng
                                        </c:if>
                                        <c:if test="${item.status == 1}">
                                            Tạm khóa
                                        </c:if>
                                    <td>
                                        <a href="edit?id=<c:out value='${item.id}'/>">Sửa</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#" onclick="deleteSubject(${item.id})">Xóa</a>                     
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </jsp:attribute>
</templace:template>