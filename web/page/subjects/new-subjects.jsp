<%-- 
    Document   : new-subjects
    Created on : May 28, 2019, 9:37:20 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <h4>Thêm mới môn học</h4>
                </div>
            </div>
            <c:if test="${item.id != null}">
                <form action="update" method="post">
                </c:if>
                <c:if test="${item.id == null}">
                    <form action="insert" method="post">
                    </c:if>
                    <c:if test="${error != null}">
                        <div class="row">
                            <div class="col-lg-12">
                                <h6 style="color: red">${error}</h5>
                            </div>
                        </div>
                    </c:if>
                    <div>
                        <input type="hidden" name="id"  value="<c:out value='${item.id}'/>"  />
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Mã môn học">Mã môn học <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <c:if test="${item.subjectCode == null}">
                                        <input disabled="true" type="text" name="subjectCode" class="form-control" value="<c:out value='${subjectCode}'/>" />
                                    </c:if>
                                    <c:if test="${item.subjectCode != null}">
                                        <input disabled="true" type="text" value="<c:out value='${item.subjectCode}'/>" name="subjectCode" class="form-control"  />
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Tên môn học">Tên môn học <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="subjectName" value="<c:out value='${item.subjectName}'/>" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Trạng thái">Trạng thái <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select class="form-control" name="status">
                                        <c:if test="${item.status == 0}">
                                            <option value="0">Đang sử dụng</option>
                                        </c:if>
                                        <c:if test="${item.status == 1}">
                                            <option value="1">Tạm khóa</option>
                                        </c:if>
                                        <option value="0">Đang sử dụng</option>
                                        <option value="1">Tạm khóa</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-lg-12 text-right">
                            <button type="submit" class="btn btn-success">Lưu</button>
                        </div>
                    </div>
                </form>
        </div>
    </jsp:attribute>
</templace:template>