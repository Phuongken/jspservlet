<%-- 
    Document   : new-mark
    Created on : May 28, 2019, 9:38:23 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <h4>Thêm mới điểm</h4>
                </div>
            </div>
            <c:if test="${item.id != null}">
                <form action="update" method="post">
                </c:if>
                <c:if test="${item.id == null}">
                    <form action="insert" method="post">
                    </c:if>
                    <c:if test="${error != null}">
                        <div class="row">
                            <div class="col-lg-12">
                                <h6 style="color: red">${error}</h5>
                            </div>
                        </div>
                    </c:if>
                    <div>
                        <input type="hidden" name="id"  value="<c:out value='${item.id}'/>"  />
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Mã môn học">Mã môn học <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="subjectId" class="form-control">
                                        <c:if test="${item.subjectId == null}">
                                            <option value="-1">Chọn mã môn học</option>
                                        </c:if>
                                        <c:if test="${item.subjectId != null}">
                                            <option value="${item.subjectId}">${item.subjectId}</option>
                                        </c:if>
                                        <c:forEach items="${listSubjects}" var="s">
                                            <option value="${s.subjectCode}">${s.subjectCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Mã sinh viên">Mã sinh viên <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <select name="studentId" class="form-control">
                                        <c:if test="${item.studentId == null}">
                                            <option value="-1">Chọn mã sinh viên</option>
                                        </c:if>
                                        <c:if test="${item.studentId != null}">
                                            <option value="${item.studentId}">${item.studentId}</option>
                                        </c:if>
                                        <c:forEach items="${listStudent}" var="s">
                                            <option value="${s.studentCode}">${s.studentCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class="control-lable" title="Điểm">Điểm <span class="require">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" name="mark" class="form-control"  value="<c:out value='${item.mark}'/>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="row margin-top-10">
                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label class="control-lable" title="Trạng thái">Trạng thái <span class="require">*</span></label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="text" name="status" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="row margin-top-10">
                        <div class="col-lg-12 text-right">
                            <button type="submit" class="btn btn-success">Lưu</button>
                        </div>
                    </div>
                </form>
        </div>
    </jsp:attribute>
</templace:template>
