<%-- 
    Document   : list-mark
    Created on : May 28, 2019, 9:38:11 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="templace" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<templace:template title="Trang chủ">
    <jsp:attribute name="content">
        <div class="col-lg-12" >
            <div class="row mt-5" style="margin-top: 10px">
                <div class="col-lg-12">
                    <a href="getForm" class="btn btn-success">Thêm mới</a>
                </div>
            </div>
            <div class="row margin-top-10">
                <div class="col-lg-12">
                    <table class="table">
                        <thead class="tableClass">
                            <tr>
                                <th>Mã môn học</th>
                                <th>Mã sinh viên</th>
                                <th>Điểm</th>
                                <th>Trạng thái</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${listMark}">
                                <tr>
                                    <td><c:out value="${item.subjectId}"/></td>
                                    <td><c:out value="${item.studentId}"/></td>
                                    <td><c:out value="${item.mark}"/></td>
                                    <td><c:out value="${item.status}"/></td>
                                    <td>
                                        <a href="edit?id=<c:out value='${item.id}'/>">Sửa</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#" onclick="deleteMark(${item.id})">Xóa</a>                     
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </jsp:attribute>
</templace:template>