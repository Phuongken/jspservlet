<%-- 
    Document   : newjsp
    Created on : May 26, 2019, 8:14:28 PM
    Author     : Phuong Ken
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Đăng nhập</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <form action="login" method="post" class="mt-5">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-4">
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 p-2" style="border: 1px solid #bdbdbd; border-radius: 3px;">
                        <h2 class="text-center">Đăng nhập</h2>
                        <div class="row mt-1 mb-2">
                            <div class="col-lg-4">
                                <label class="control-lable">Tài khoản</label>
                            </div>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="userName"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label class="control-lable">Mật khẩu</label>
                            </div>
                            <div class="col-lg-8">
                                <input type="password" class="form-control" name="password"/>
                            </div>
                        </div>
                        <c:if test="${error != null}">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 style="color: red">${error}</h5>
                                </div>
                            </div>
                        </c:if>
                        <div class="row mt-3">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-success">Đăng nhập</button>
                                <button class="btn btn-dark">Đăng ký</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-4">
                    </div>   
                </div>
            </form>
        </div>
    </body>
</html>
