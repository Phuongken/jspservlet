/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function provindChange() {
    var idProvind = document.getElementById("provindId").value;
    $.ajax({
        type: "get",
        url: "../address",
        data: {acctionName: "provind", id: idProvind},
        success: function (response) {
            debugger
            var result = "";
            $.each(JSON.parse(response), function (i, value) {
                result += '<option value="' + value.id + '">' + value.districtName + '</option>';
            });
            document.getElementById("districtId").innerHTML = result;
        },
        dataType: 'text'
    });
}
function districtChange() {
    var idDistrict = document.getElementById("districtId").value;
    $.ajax({
        type: "get",
        url: "../address",
        data: {acctionName: "district", id: idDistrict},
        success: function (response) {
            var result = "";
            $.each(JSON.parse(response), function (i, value) {
                result += '<option value="' + value.id + '">' + value.townName + '</option>';
            });
            document.getElementById("townId").innerHTML = result;
        },
        dataType: 'text'
    });
}