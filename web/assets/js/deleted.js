/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function deleteStudent(id) {
    debugger
    var confirmMessage = confirm("Bạn có chắc muốn xóa sinh viên này không");
    if (confirmMessage) {
        $.ajax({
            type: "get",
            url: "../student/delete",
            data: {id: id},
            success: function (response) {
                if (response=="true") {
                    alert("Xóa sinh viên thành công");
                    window.location.assign("../student/list");
                } else{
                    alert("Xóa sinh viên không thành công");
                }
            },
            dataType: 'text'
        });
    }
}

function deleteSubject(id) {
    debugger
    var confirmMessage = confirm("Bạn có chắc muốn xóa môn học này không");
    if (confirmMessage) {
        $.ajax({
            type: "get",
            url: "../subjects/delete",
            data: {id: id},
            success: function (response) {
                if (response=="true") {
                    alert("Xóa môn học thành công");
                    window.location.assign("../subjects/list");
                } else{
                    alert("Xóa môn học không thành công");
                }
            },
            dataType: 'text'
        });
    }
}

function deleteMark(id) {
    debugger
    var confirmMessage = confirm("Bạn có chắc muốn xóa điểm  này không");
    if (confirmMessage) {
        $.ajax({
            type: "get",
            url: "../mark/delete",
            data: {id: id},
            success: function (response) {
                if (response=="true") {
                    alert("Xóa điểm thành công");
                    window.location.assign("../mark/list");
                } else{
                    alert("Xóa điểm không thành công");
                }
            },
            dataType: 'text'
        });
    }
}

function deleteUser(id) {
    debugger
    var confirmMessage = confirm("Bạn có chắc muốn xóa tài khoản  này không");
    if (confirmMessage) {
        $.ajax({
            type: "get",
            url: "../user/delete",
            data: {id: id},
            success: function (response) {
                if (response=="true") {
                    alert("Xóa tài khoản thành công");
                    window.location.assign("../user/list");
                } else{
                    alert(response);
                }
            },
            dataType: 'text'
        });
    }
}